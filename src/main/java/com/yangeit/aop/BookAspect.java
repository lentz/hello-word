package com.yangeit.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Aspect
@Slf4j
public class BookAspect {

    @Before("execution( * com.yangeit.controller.*.*(..))")
    public void Before(JoinPoint joinPoint){
        log.info("aop before 参数：{}", Arrays.toString(joinPoint.getArgs()));
    }


    @After("execution( * com.yangeit.controller.*.*(..))")
    public void After(JoinPoint joinPoint){
        log.info("aop After 参数：{}", Arrays.toString(joinPoint.getArgs()));
    }

}
