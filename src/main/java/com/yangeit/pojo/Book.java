package com.yangeit.pojo;

import lombok.Data;

@Data
public class Book {

    String name;
    Long id;
}
