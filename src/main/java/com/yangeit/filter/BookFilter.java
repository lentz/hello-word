package com.yangeit.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter
@Slf4j
public class BookFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


        log.info("过滤器执行之前 doFilter ");

        filterChain.doFilter(servletRequest,servletResponse);

        log.info("过滤器执行之后 doFilter");

    }
}
