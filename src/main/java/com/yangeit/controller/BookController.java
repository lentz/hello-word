package com.yangeit.controller;


import com.yangeit.pojo.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/books")
public class BookController {

    /**
     * 路径：localhost:8080/books?name=haha
     * @param name
     */
    @GetMapping
    public String getbook(String name){
        log.info("controller 方法getbook 参数：{}",name);
        return "dyb:"+name;
    }

    @PostMapping
    public Book getBook2(@RequestBody Book book){
        log.info("controller 方法getBook2 参数：{}",book);
        return book;
    }


//    如果id为long类型 传给前端js 会精度丢失，案例如下
//    在线编辑器：https://www.bejson.com/runcode/javascript/
//    示例代码：
//
//var cont={
//    "id": 1594176876400726018,
//    "name": "222"
//}
//
//console.log(cont);
}
